var checkBox = {
	addClassTo : function (toEl,classNm) {
    	toEl.setAttribute('class',toEl.className+' '+classNm);
	},
	delClassFrom : function (fromEl,classNm) {
		var _class = fromEl.getAttribute('class');
		var _classResult = _class.replace(classNm,'');
		fromEl.setAttribute('class',_classResult);
	},
	select : function  (selCh) {
		var checkBoxes = document.querySelectorAll(selCh);
		var that = this;
		for (var i = checkBoxes.length - 1; i >= 0; i--) {
			checkBoxes[i].addEventListener('click',function () {
				var parentNode = this.parentNode.parentNode;
				(this.checked) ? that.addClassTo(parentNode, 'selectedOpt') : that.delClassFrom(parentNode,'selectedOpt');
			},false);
		};
	}
}