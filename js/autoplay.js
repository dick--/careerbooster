var Autoplay = {
	_current : 0,
	hit : function (el) {
		el.checked = true;
	},
	addClassTo : function (toEl,classNm) {
    	toEl.setAttribute('class',toEl.className+' '+classNm);
	},
	delClassFrom : function (fromEl,classNm) {
		var _class = fromEl.getAttribute('class');
		var _classResult = _class.replace(classNm,'');
		fromEl.setAttribute('class',_classResult);
	},
	checkRadios : function (arr) {
		for (var i = arr.length - 1; i >= 0; i--) {
			if (arr[i].checked == true) {
				_current = i;
			}
		};
		return _current;
	},
	play : function (el) {
		var _arr = document.querySelectorAll(el);
		var _i = this.checkRadios(_arr);
		if (_i == 2) {
			this.hit(_arr[0]);
		} else {
			this.hit(_arr[_i + 1]);
		}
	},
	restartAnimation : function (animClassNm,animNodeNm,animNextSiblingNm) {
		var _obj = this;
		var _arr = document.querySelectorAll(animNodeNm);
		for (var j = _arr.length - 1; j >= 0; j--) {
			var _parent;
			var _node;
			_obj.delClassFrom(_arr[j],' '+animClassNm);
			_node = _arr[j].cloneNode(true);
			_parent = _arr[j].parentNode;
			_parent.removeChild(_parent.querySelector(animNodeNm));
			_parent.insertBefore(_node,_parent.querySelector(animNextSiblingNm));
			_obj.addClassTo(_parent.querySelector(animNodeNm),animClassNm);
		};
	},
	init : function (el,timeout) {
		var _obj = this;
		var _id = setInterval(function() { _obj.play(el);
			_obj.restartAnimation('anim','.innerAction','.content');
		 }, timeout);
		var _arr = document.querySelectorAll(el);
		for (var i = _arr.length - 1; i >= 0; i--) {
			_arr[i].addEventListener("click",function () {
				_obj.restartAnimation('anim','.innerAction','.content');
				clearInterval(_id);
			},false);
		};
	}
}