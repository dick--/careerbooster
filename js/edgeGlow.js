var EdgeGlow = {
	spoiler : function (inputEl) {
			var _paragraph = inputEl.previousElementSibling;
			if (_paragraph.getAttribute('class').indexOf("withGlow") == -1) {
				_paragraph.setAttribute('class',_paragraph.className+' '+'withGlow');
			} else {
				_paragraph.setAttribute('class','edgeGlow');
			}
		},
	init : function (elClass) {
			var _elements = document.querySelectorAll(elClass);
			for (var i = _elements.length - 1; i >= 0; i--) {
				_elements[i].addEventListener("click", function (){ EdgeGlow.spoiler(this); return false; },false);
			}
		}
}